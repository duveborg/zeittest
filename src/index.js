import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ApolloClient, { createNetworkInterface } from 'apollo-client'
import { ApolloProvider } from 'react-apollo'


const networkInterface = createNetworkInterface({
    uri: 'https://web-backend-dev.zeitgold.com/graphiql',
})

const client = new ApolloClient({
    networkInterface,
})

ReactDOM.render(
    <ApolloProvider client={client}>
        <App />
    </ApolloProvider> 
, document.getElementById('root'));
