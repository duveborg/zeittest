import React, { Component } from 'react';
import './App.css';
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const SuppliersQuery = gql`
query {
  business(id: "QnVzaW5lc3NOb2RlOmI0OTllOGVlLTliZWUtNGE5NC1iNGJjLTZkZmRkNzI5ZTFkYQ==") {
    businessSuppliers {
      edges {
        node {
          displayName
        }
      }
    }
  }
}`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  onClick() {
    this.setState({
      selectorOpen: true,
      inputText: ""
    })
  }

  onInputChange(event) {
    this.setState({
      inputText: event.target.value
    })
  }

  selectItem(item) {
    this.setState({
      selectedItem: item.node.displayName,
      selectorOpen: false,
      inputText: ""
    })
  }

  closeSelector(event) {
    event.preventDefault();
    this.setState({selectorOpen: false})
  }

  clearInput() {
    this.setState({inputText: ""})
  }

  render() {
    if (this.props.data.loading) return <div className="loading">Loading</div>

    const listItems = this.props.data.business.businessSuppliers.edges.filter(item => {
      return !this.state.inputText || item.node.displayName.toLowerCase().startsWith(this.state.inputText.toLowerCase())
    })

    return (
      <div className="app">
        <button className="dropdownButton" onClick={this.onClick.bind(this)}>
          { this.state.selectedItem ? this.state.selectedItem : "Select Business supplier" }
          <img className="dropdownButtonIcon" src="https://image.flaticon.com/icons/png/512/3/3907.png"></img>
        </button>

        { this.state.selectorOpen && 
          <div className="selector">

            <div className="selectorInput">
              <img className="searchIcon" src="https://image.flaticon.com/icons/svg/54/54481.svg"></img>
              <input value={this.state.inputText} onChange={this.onInputChange.bind(this)}/>
              <img className="clearIcon" onClick={this.clearInput.bind(this)} src="https://cdn1.iconfinder.com/data/icons/basic-ui-7/100/Artboard_16-512.png"/>
            </div>

            <a className="closeLink" onClick={this.closeSelector.bind(this)}>Close</a>

            { listItems.length == 0 ? <div className="noMatches">No matches</div> : "" }

            <ul className="list">
              { listItems.map(item => 
                <li className="listItem" 
                    onClick={() => this.selectItem(item)}>
                    { item.node.displayName }
                    { item.node.displayName === this.state.selectedItem ? <img className="selectedItem" src="https://mpng.pngfly.com/20180413/pfq/kisspng-check-mark-computer-icons-clip-art-checkmark-5ad13271a848e1.9645528515236593776893.jpg"></img> : "" }
                </li>) }
            </ul>
          </div>
        }
      </div>
    );
  }
}

export default graphql(SuppliersQuery)(App)
